
==============================
About
==============================

Flickr Insert allows you to easily insert Flickr sets into content throughout your site, whether it be nodes, blocks or even comments by the use of [flickrinsert||...] tags.

==============================
Installation
==============================

 1. Drop the flickrinsert folder into the modules directory (/sites/all/modules/)
 2. Enable Flickr Insert module (?q=/admin/build/modules)

Required Modules:
------------------------------

 - Flickr API (http://drupal.org/project/flickrapi)

==============================
Known Issues
==============================

This module is in very early development so take care!

 - Issue with incorrect interpretation of photo sizes (main issue with Large and Original photos).

==============================
The FlickrInsert Tag
==============================

[flickrinsert||type==$type==$id||size==$size||num==$num||rand==$rand||pager==$pager||link==$link]

$type: photoset or photo
$id: ID of photoset or photo
$size: sq (75px), t (100px), s (240px), m (500px), l (1024px), o (original)
$num: number of photos to display (per page)
$rand: 0 for false, 1 for true; randomise display photos - will automatically disable the pager
$pager: Use a pager so the user can navigate the whole photoset
$link: none (Don't Link), page (Flickr Photo Page), sq (75px), t (100px), s (240px), m (500px), l (1024px), o (original)

==============================
The Future  
==============================

If you have any questions, issues, or feature suggestions then please do leave feedback on the project page (http://drupal.org/project/flickrinsert)