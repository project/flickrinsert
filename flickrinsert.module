<?php

//Make sure pager elements get a unique ID.
$element = 0;

/**
 * Implementation of hook_help().
 */
function flickrinsert_help($path, $arg) {
  $output = '';
  switch ($path) {
    case "admin/help#flickrinsert":
      $output = '<p>'. t("Insert photo sets into Flickr.") .'</p>';
      break;
  }
  return $output;
}

/**
 * Implementation of hook_init().
 */
function flickrinsert_init() {
  drupal_add_css(drupal_get_path('module', 'flickrinsert') .'/theme/flickrinsert.css');
}


/** ========================================
 * FILTER TAGS
 */


/**
 * Implementation of hook_filter().
 */
function flickrinsert_filter($op, $delta = 0, $format = -1, $text = '') {
  if ($op == 'list') {
    return array(
      0 => t('Flickr Insert'),
    );
  }

  switch ($delta) {
    case 0:
      switch ($op) {
        case 'description':
          return t('Replaces Flickr Insert tags with photosets and photos from Flickr (e.g. [flickrinsert||...]).');

        case 'no cache':
          return TRUE;

        case 'prepare':
          return $text;

        case 'process':
          $processed = FALSE;
          foreach (flickrinsert_get_tags($text) as $unexpanded_macro => $macro) {
            $expanded_macro = flickrinsert_render_tag($macro);
            $text           = str_replace($unexpanded_macro, $expanded_macro, $text);
            $processed      = FALSE;
          }
          return $text;
      }
      break;
  }
}

/**
 * Implementation of hook_filter_tips().
 */
function flickrinsert_filter_tips($delta, $format, $long = false) {
  if ($long) {
    return t('To post Flickr photos use: [flickrinsert||type==photo==0000||size==m||link==none] or for photosets: [flickrinsert||type==photoset==0000||size==sq||num==5||rand==0||pager==1||link==page].');
  }
  else {
    return t('To post Flickr photos use: [flickrinsert||type==photo==0000||size==m||link==none] or for photosets: [flickrinsert||type==photoset==0000||size==sq||num==5||rand==0||pager==1||link==page].');
  }
}

/**
 * Get Flickr Insert tags.
 */
function flickrinsert_get_tags($text) {
  $m = array();
  preg_match_all('/ \[ ( [^\[\]]+ )* \] /x', $text, $matches);
  // Don't process duplicates.
  $tag_match = (array) array_unique($matches[1]);
  
  foreach ($tag_match as $macro) {
    $current_macro = '['. $macro .']';
    $param = array_map('trim', explode('||', $macro));
    // The first macro param is assumed to be the function name.
    $func_name = array_shift($param);
    if ($func_name == 'flickrinsert') {
      $vars = array();
      foreach ($param as $p) {
        $parts = explode("==", $p);
        $vars[$parts[0]] = $parts;
      }
      // The full unaltered filter string is the key for the array of filter
      // attributes.
      $m[$current_macro] = $vars;
    }
  }
  
  return $m;
}

/**
 * Render tags.
 */
function flickrinsert_render_tag($attr = array()) {
  //Variables
  $type     = (string)  $attr['type'][1];   //Either Photo or Photoset
  $id       = (int)     $attr['type'][2];   //ID of Photo or Photoset
  $size     = (string)  $attr['size'][1];   //Flickr image size
  $num      = (int)     $attr['num'][1];    //Number of photos to display
  $random   = (bool)    $attr['rand'][1];   //Randomise image?
  $pager    = (bool)    $attr['pager'][1];  //Show pager?
  $link     = (string)  $attr['link'][1];   //Link photo?
  
  //Validation
  if($size != 'sq' && $size != 't' && $size != 's' && $size != 'm' && $size != 'o') {
    return 'Size is not valid.';
  }
  if($num > 500) {
    return 'You cannot display more than 500 photos at a time.';
  }
  if($link != 'none' && $link != 'page' && $link != 'sq' && $link != 't' && $link != 's' && $link != 'm' && $link != 'o') {
    return 'Link is not valid.';
  }
  
  //Translation
  switch ($size) {
    case 'sq':
      $size = 0;
      break;
    case 't':
      $size = 1;
      break;
    case 's':
      $size = 2;
      break;
    case 'm':
      $size = 3;
      break;
    case 'o':
      $size = 4;
      break;
  }
  
  if ($type == 'photo') {
    return flickrinsert_photo($id, $size, NULL, $link);
  }
  elseif ($type == 'photoset') {
    return flickrinsert_photoset($id, $size, $num, $random, $pager, $link);
  }
}

/**
 * Fetch Flickr API object.
 */
function flickrinsert_invoke_api() {
  global $f;
  if(!isset($f) || $f == '') {
    $f = flickrapi_phpFlickr();
  }
  
  return $f;
}

/**
 * Process Flickr photo.
 */
function flickrinsert_photo($id, $size, $user, $link) {
  //Make API call
  $f = flickrinsert_invoke_api();
  
  $photo = $f->photos_getSizes($id);
  
  if($user == NULL) {
    $photo_info = $f->photos_getInfo($id);
    $user = $photo_info['owner']['nsid'];
  }
  
  switch ($link) {
    case 'none':
      $link = NULL;
      break;
    case 'page':
      $link = 'http://www.flickr.com/photos/'.$user.'/'.$id;
      break;
    case 'sq':
      $link = $photo[0]['source'];
      break;
    case 't':
      $link = $photo[1]['source'];
      break;
    case 's':
      $link = $photo[2]['source'];
      break;
    case 'm':
      $link = $photo[3]['source'];
      break;
    case 'o':
      $link = $photo[4]['source'];
      break;
  }
  
  return theme('flickrinsert_photo', $photo[$size]['source'], $photo[$size]['height'], $photo[$size]['width'], $link);
}

/**
 * Process Flickr photoset.
 */
function flickrinsert_photoset($id, $size, $num, $random, $pager, $link) {
  global $pager_page_array, $pager_total, $element;
  $element++;
  //Make API call
  $f = flickrinsert_invoke_api();
  
  $overview = $f->photosets_getInfo($id);
  $user = $overview['owner'];
  
  if (!isset($_GET['page']) || $_GET['page'] == '') {
    $page = 0;
  }
  else {
    $page_parts = explode(',', $_GET['page']);
    $page = $page_parts[$element];
  }
  
  $pager_page_array[$element] = $page;
  $pager_total[$element] = (int) ceil($overview['photos']/$num);
  
  //Random photos wanted?
  if ($random == FALSE) {
    $photos = $f->photosets_getPhotos($id, NULL, 1, $num, $pager_page_array[$element]+1, 'photos');
    $photos = $photos['photo'];
  }
  else {
    $photos = $f->photosets_getPhotos($id, NULL, 1, 500, 1, 'photos');
    $photos_random = array_rand($photos['photo'], $num);
    $photos_new = array();
    foreach($photos_random as $random_id) {
      $photos_new[] = $photos['photo'][$random_id];
    }
    $photos = $photos_new;
    $pager = FALSE;
  }
  
  if ($pager == TRUE) {
    $pager_real = theme('pager', NULL, $num, $element);
  }
  else {
    $pager_real = '<div class="clear"></div>';
  }
  
  return '<div class="flickrinsert_photoset">' . theme('flickrinsert_photoset', $photos, $size, $user, $link).$pager_real . '</div>';
}


/** ========================================
 * THEME
 */


/**
 * Implementation of hook_theme().
 */
function flickrinsert_theme() {
  return array(
    'flickrinsert_photo' => array(
      'arguments' => array(
        'url' => NULL,
        'height' => NULL,
        'width' => NULL,
        'link' => NULL,
      )
    ),
    'flickrinsert_photoset' => array(
      'arguments' => array(
        'photos' => array(),
        'size' => 2,
        'user' => NULL,
        'link' => NULL,
      ),
    ),
  );
}

/**
 * Theme Flickr photo.
 */
function theme_flickrinsert_photo($url, $height, $width, $link) {
  $attributes = array(
    'width' => $width,
    'height' => $height,
    'class' => 'flickrinsert_photo',
  );
  
  if ($link == NULL) {
    return theme('image', $url, 'Flickr Insert Image', NULL, $attributes, FALSE);
  }
  else {
    return '<a href="'.$link.'" class="flickrinsert_link">'.theme('image', $url, 'Flickr Insert Image', NULL, $attributes, FALSE).'</a>';
  }
}

/**
 * Theme Flickr photoset.
 */
function theme_flickrinsert_photoset($photos, $size, $user, $link) {
  $output = '<ul class="photos">';
  foreach($photos as $photo) {
    $output .= '<li>' . flickrinsert_photo($photo['id'], $size, $user, $link) . '</li>';
  }
  
  return $output . '</ul>';
}