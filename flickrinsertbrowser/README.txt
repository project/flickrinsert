
==============================
About
==============================

Flickr Insert Browser is a plugin for FCKeditor that makes it easy to browse your photosets, change various display options and then insert the tag into your node, comment or block.

==============================
Installation
==============================

 1. Drop the flickrinsert folder into the modules directory (/sites/all/modules/)
 2. Enable Flickr Insert Browser module (?q=/admin/build/modules).
 3. Enter your Flickr user ID so it knows which photosets to return.

Required Modules:
------------------------------

 - FCKeditor (http://drupal.org/project/fckeditor)
 - Flickr Insert (http://drupal.org/project/fckeditor)

Installing the FCKeditor plugin:
------------------------------

 1. Copy the flickrinsert directory from /modules/flickinsert/flickrinsertbrowser/plugins/ into /modules/fckeditor/plugins/
 2. Edit /modules/fckeditor/fckeditor.config.js - insert the line: FCKConfig.Plugins.Add( 'flickrinsert' ) ; below line 19

==============================
Known Issues
==============================

This module is in very early development so take care!

 - Can't browse and insert single photos yet.
 - Incompatible if Image Assist is enabled.
 - Can't edit existing Flickr Insert tags.

==============================
The Future  
==============================

If you have any questions, issues, or feature suggestions then please do leave feedback on the project page (http://drupal.org/project/flickrinsert)