
/**
 * @file flickrinsert.js
 *
 * The javascript for Flickr Insert
 */

/**
 * FCKEditor insert function
 */
function FlickrInsert_InsertPhotoset(type, id, size, num, rand, pager, link) {
  var oImage = dialog.Selection.GetSelectedElement() ;
  
  if ( oImage && oImage.tagName.toLowerCase() != 'img' )
  oImage = null ;
  
  var bHasImage = ( oImage != null ) ;
  
  // Get the active link.
  var oLink = dialog.Selection.GetSelection().MoveToAncestorNode( 'A' ) ;
  oEditor.FCKUndo.SaveUndoStep() ;
  
  if ( !bHasImage )
  {
    oImage = oEditor.FCK.InsertElement( 'img' ) ;
  }
  
   // [flickrinsert||type==photoset==72157613429245274||size==sq||num==4||rand==0||pager==1||link==o] 
  $(oImage).attr("src", flickrinsert_path + '/plugins/flickrinsert/FlickrInsertPreview.jpg');
  $(oImage).attr("alt", 'Flickr Insert');
  $(oImage).attr("class", 'flickr_insert_preview');
  $(oImage).attr("_fck_fi", "type=="+type+"=="+id+"||size=="+size+"||num=="+num+"||rand=="+rand+"||pager=="+pager+"||link=="+link);
  
   dialog.CloseDialog();
}

/**
 * Prepare links in the browser for insert overlay
 */
function FlickrInsert_PrepareLinks() {
  //Insert button
  $(".insert").click(function() {
    var type = $("#edit-type").val();
    var id = $("#edit-id").val();
    var size = $("#edit-size").val();
    var num = $("#edit-num").val();
    var rand = $("#edit-rand").val();
    var pager = $("#edit-pager").val();
    var link = $("#edit-link").val();
    
    FlickrInsert_InsertPhotoset(type, id, size, num, rand, pager, link);
    return false;
  });
}

/**
 * Make things happen on page load
 */
$(document).ready(function(){
  FlickrInsert_PrepareLinks();
});