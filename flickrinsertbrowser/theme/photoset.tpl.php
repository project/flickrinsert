<?php

/**
 * @file
 * Template for Photoset details window.
 */
?>
<div id="photoset">
  <div class="options">
    <p class="info"><?php print t('Customise your photoset before insert:'); ?></p>
    <?php print $form; ?>
  </div>
  <div class="overview">
    <div class="thumbnail"><?php print $thumbnail; ?></div>
    <div class="title"><?php print $title; ?></div>
    <div class="description"><?php print $description; ?></div>
  </div>
</div>
<div id="footer">
  <a href="#" class="insert"><?php print t('Insert Flickr Photoset'); ?></a>
</div>