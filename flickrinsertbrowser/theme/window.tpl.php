<?php

/**
 * @file
 * Template for Flickr Insert's main window.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php print t('Flickr Insert'); ?></title>
    <?php print $css; ?>
    <script language="javascript">
      var dialog = window.parent ;
      var oEditor = dialog.InnerDialogLoaded() ;
    </script>
  </head>
  <body>
    <div id="main"><?php print $body; ?></div>
    <?php print $javascript; ?>
  </body>
</html>