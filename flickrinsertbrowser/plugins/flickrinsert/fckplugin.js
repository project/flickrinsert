var FCKBasePath = top.window.Drupal.settings.basePath;

//Register Flickr Insert Command
FCKCommands.RegisterCommand('FlickrInsert', new FCKDialogCommand('FlickrInsert', 'Flickr Insert', FCKBasePath + 
'?q=flickrinsert/browser&app=FCKEditor', 667, 488));

//Register Flickr Insert Toolbar Button
var oIBItem = new FCKToolbarButton( 'FlickrInsert', 'Flickr Insert', null, FCK_TOOLBARITEM_ONLYICON, true, true ) ;
oIBItem.IconPath = FCKConfig.PluginsPath + 'flickrinsert/flickr.gif';
FCKToolbarItems.RegisterItem( 'FlickrInsert', oIBItem ) ;

//Add Flickr Insert Toolbar Button Automatically
addToolbarElement('FlickrInsert', 'Basic', 0);
addToolbarElement('FlickrInsert', 'DrupalBasic', 0);
addToolbarElement('FlickrInsert', 'Default', 4);
addToolbarElement('FlickrInsert', 'DrupalFiltered', 0);
addToolbarElement('FlickrInsert', 'DrupalFull', 0);

//Override Context Menu On Flickr Insert Tags
FCK.ContextMenu.RegisterListener({
  AddItems : function( contextMenu, tag, tagName )
  {
    if (tagName.toLowerCase() == 'img') {
      if (tag.getAttribute('_fck_fi')) {
        contextMenu.RemoveAllItems();
        contextMenu.AddItem( 'FlickrInsert', 'Flickr Insert' , FCKConfig.PluginsPath + 'flickrinsert/flickr.gif');
      }
    }
  }
});

//Add jQuery Library
function appendScripts() {
  var jquery = document.createElement("script");
  jquery.type = "text/javascript";
  jquery.src = FCKBasePath + "misc/jquery.js";
  document.getElementsByTagName("head")[0].appendChild(jquery);
}
appendScripts();

//Save reference to the internal FCKeditor TagProcessor
//It must be called to process _fcksavedurl [#352704]
//FIXME: bad bad bad code, infinite loop when imgassist plugin is enabled
var FCKXHtml_TagProcessors_Img = FCKXHtml.TagProcessors['img'];

// We must process the IMG tags, change <img...> to [flickrinsert...]
FCKXHtml.TagProcessors['img'] = function( node, htmlNode )
{
  node = FCKXHtml_TagProcessors_Img( node, htmlNode ) ;

  if ( htmlNode.getAttribute('_fck_fi') ) {
    var IBString = FCKTools.FlickrInsertCode ( htmlNode );
    if( IBString )
      node = FCKXHtml.XML.createTextNode(IBString) ;
  }
  else{
    FCKXHtml._AppendChildNodes( node, htmlNode, false ) ;
  }
  return node ;
}

// Convert <img...> tag into [flickrinsert...]
FCKTools.FlickrInsertCode = function( IBObj )
{
  var IBString = [];
  if ( IBObj.getAttribute('_fck_fi') ) {
    IBString.push('[flickrinsert||');
    IBString.push(IBObj.getAttribute('_fck_fi'));
    IBString.push(']');
  }
  else {
    return false;
  }
  return IBString.join("");
}

// Find all occurences of [flickrinsert...] and call FlickrInsertDecode
var FCKFlickrInsertProcessor = FCKDocumentProcessor.AppendNew() ;
FCKFlickrInsertProcessor.ProcessDocument = function( document )
{
  // get all elements in FCK document
  var elements = document.getElementsByTagName( '*' ) ;

  // check every element for childNodes
  var i = 0;
  while (element = elements[i++]) {
    var nodes = element.childNodes;

    var j = 0;
    while (node = nodes[j++]) {
      if (node.nodeName == '#text') {
        var index = 0;
        while (aPlaholders = node.nodeValue.match( /\[flickrinsert[^\[\]]+\]/g )) {
          index = node.nodeValue.indexOf(aPlaholders[0]);
          if (index != -1) {
            var oImgBr = FCKTools.FlickrInsertDecode ( aPlaholders[0] );

            var substr1 = FCK.EditorDocument.createTextNode( node.nodeValue.substring(0, index) );
            var substr2 = FCK.EditorDocument.createTextNode( node.nodeValue.substring(index + aPlaholders[0].length) );

            node.parentNode.insertBefore( substr1, node ) ;
            node.parentNode.insertBefore( oImgBr, node ) ;
            node.parentNode.insertBefore( substr2, node ) ;

            node.parentNode.removeChild( node ) ;
            if (node) node.nodeValue='';
          }
        }
      }
    }
  }
}

// [flickrinsert...] -> <img...>
FCKTools.FlickrInsertDecode = function( IBString )
{
  var IBTmp = FCK.EditorDocument.createElement( 'img' );
  var IBarg = IBString.match( /\[flickrinsert\|\|\s*([^\]]*?)\]/ );
  $(IBTmp).attr("src", FCKConfig.PluginsPath + 'flickrinsert/FlickrInsertPreview.jpg');
  $(IBTmp).attr("alt", 'Flickr Insert');
  $(IBTmp).attr("class", 'flickr_insert_preview');
  $(IBTmp).attr("_fck_fi", IBarg[1]);
  return IBTmp;
}